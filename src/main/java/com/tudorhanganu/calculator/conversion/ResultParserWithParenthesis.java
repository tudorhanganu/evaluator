package com.tudorhanganu.calculator.conversion;

import com.tudorhanganu.calculator.exceptions.DivisionByZero;
import com.tudorhanganu.calculator.exceptions.InvalidInputQueueString;
import com.tudorhanganu.calculator.exceptions.NonBinaryExpression;
import com.tudorhanganu.calculator.exceptions.NonMatchingParenthesis;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * @author Tudor Hanganu
 */
public class ResultParserWithParenthesis {
    
    /**
     *
     * Method to convert the infix queue string to postfix
     * 
     * @param infixQueue
     * @return
     * @throws NonMatchingParenthesis
     * @throws InvalidInputQueueString
     * @throws NonBinaryExpression
     */
    public Queue<String> convertInfixToPostfix(Queue<String> infixQueue) throws NonMatchingParenthesis, InvalidInputQueueString, NonBinaryExpression {
        
        Stack<String> stack = new Stack<String>();
        Queue<String> postfix = new LinkedList<String>();
        
        // double checking for )( or x( creating new list, adding multiplication sign in between.
        String previousValue = "";
        
        Queue<String> infix = new LinkedList<String>();
        
        for (String string : infixQueue){
            if (string.equals(")") || isNumeric(string)){
                previousValue = string;
            }
            else if (string.equals("(")){
                if (!previousValue.equals("")){
                    infix.add("*");
                }
            }
            else {
                previousValue = "";
            }
            infix.add(string);
            
        }
        System.out.println(infix.toString());
        previousValue = "";
        
        repeatingOperator(infix);
        repeatingOperand(infix);
        endsInOperator(infix.toString());
        
        // GOING THROUGH THE INFIX LIST
        for (String string : infix){

            // CHECKING FOR -( AND +(
            if (string.length() > 1){
                if (string.equals("-(")){
                    string = "-1";
                    stack.push("*");
                    stack.push("(");
                }
                else if (string.equals("+(")){
                    string = "(";
                }
            }
            
            // CHECKING FOR INVALID DATA
            if (!string.equals("+") && !string.equals("-") && !string.equals("*") && !string.equals("/") && !isNumeric(string) && !string.equals("(") && !string.equals(")")){
                throw new InvalidInputQueueString("Invalid data passed -> " + string);
            }
            
            // CHECKING FOR NUMBERS
            if (isNumeric(string))
                postfix.add(string);
            
            // CHECKING FOR OPENING PARANTHESIS
            else if (string.equals("("))
                stack.push(string);
            
            // CHECKING FOR CLOSING PARANTHESIS
            else if (string.equals(")")){
                if (stack.contains("(")){
                    String pop = stack.pop();
                    while (!pop.equals("(")){
                        postfix.add(pop);
                        pop = stack.pop();
                    }
                }
                else {
                    throw new NonMatchingParenthesis("Could not find opening paranthesis");
                }
            }
            // CHECKING FOR OPERATORS
            else {
                if (!stack.isEmpty() && getOperationOrder(stack.peek()) < getOperationOrder(string)){
                    stack.push(string);
                }
                else if (!stack.isEmpty() && getOperationOrder(stack.peek()) >= getOperationOrder(string)){
                    postfix.add(stack.pop());
                    stack.push(string);
                }
                else if (stack.isEmpty()){
                    stack.push(string);
                }
            }
            previousValue = string;
        }
        // ADDING REMAINDER IN STACK TO THE POSTFIX LIST
        while (!stack.isEmpty()){
            postfix.add(stack.pop());
        }
        
        correctParanthesis(postfix);
        
        return postfix;
    }
    
    /**
     * Calculates the postfix of the expression
     * 
     * @param postfixQueue
     * @return
     * @throws DivisionByZero
     */
    public String evaluatePostfix(Queue<String> postfixQueue) throws DivisionByZero { 
        
        Stack<Double> stack = new Stack<>();
        
        for (String string : postfixQueue){
                
            if(string.length()==1 && (string.charAt(0)=='+'||string.charAt(0)=='-'||string.charAt(0)=='*'||string.charAt(0)=='/')){
                double number2 = stack.pop();
                double number1 = stack.pop();
                if(string.charAt(0)=='+'){
                    double number = number1+number2;
                    stack.push(number);
                }
                else if(string.charAt(0)=='-'){
                    double number = number1-number2;
                    stack.push(number);
                }
                else if(string.charAt(0)=='*'){
                    double number = number1*number2;
                    stack.push(number);
                }
                else{
                    if (number1 == 0 || number2 == 0){
                        throw new DivisionByZero("Division by zero");
                    }
                    else {
                        double number = number1/number2;
                        stack.push(number);
                    }
                }
            }else{
                double number = Double.parseDouble(string);
                stack.push(number);
            }
        }
        return stack.peek().toString();
    }
    
    private static int getOperationOrder(String character){
        switch (character) {
            case "+": 
            case "-": 
                return 1; 

            case "*": 
            case "/": 
                return 2; 
 
            } 
        
            return -1;  
    }
 
    private static boolean isOperator(String string){
        return getOperationOrder(string) > 0;
    }
    
    private static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
    return true;
}

    private static void repeatingOperator(Queue<String> string) throws NonBinaryExpression{
        
        boolean operator = false;
        
        for (String element : string){
            if (isOperator(element)){
                if (operator){
                    throw new NonBinaryExpression("2 operators in a row");
                }
                operator = true;
            }
            else if (element.charAt(0)=='(' || element.charAt(0)==')'){}
            else {
                operator = false;
            }
        }
    }
    
    private static void repeatingOperand(Queue<String> string) throws NonBinaryExpression{
        
        boolean operand = false;
        
        for (String element : string){
            if (isNumeric(element)){
                if (operand){
                    throw new NonBinaryExpression("2 operands in a row");
                }
                operand = true;
            }
            else {
                operand = false;
            }
        }
    }

    private static void endsInOperator(String string) throws NonBinaryExpression{
        
        if (isOperator(Character.toString(string.charAt(string.length()-2)))){
            throw new NonBinaryExpression("Ends with operator");
        }
        
    }

    private static void correctParanthesis(Queue<String> string) throws NonMatchingParenthesis{
        int start = 0;
        int end = 0;
        
        for (String element : string){
            if (element.equals("(")){
                start++;
            }
            else if (element.equals(")")){
                end++;
            }
        }
        if (start != end){
            throw new NonMatchingParenthesis("Not same amount of opening/closing paranthesis");
        }
        
    }
}
